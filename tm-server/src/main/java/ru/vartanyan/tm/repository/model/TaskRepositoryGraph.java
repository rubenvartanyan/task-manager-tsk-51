package ru.vartanyan.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.model.TaskGraph;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepositoryGraph extends AbstractRepository<TaskGraph> implements ITaskRepositoryGraph {

    public TaskRepositoryGraph(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public @Nullable TaskGraph findOneById(@Nullable final String id) {
        return entityManager.find(TaskGraph.class, id);
    }

    public void remove(final TaskGraph entity) {
        TaskGraph reference = entityManager.getReference(TaskGraph.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeOneById(@Nullable final String id) {
        TaskGraph reference = entityManager.getReference(TaskGraph.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<TaskGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskGraph e", TaskGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskGraph> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskGraph e WHERE e.user.id = :userId", TaskGraph.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskGraph> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskGraph e WHERE e.user.id = :userId AND e.project.id = :projectId",
                        TaskGraph.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        entityManager
                .createQuery(
                        "DELETE FROM TaskGraph e WHERE e.user.id = :userId AND e.project.id = :projectId"
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE TaskGraph e SET e.project.id = :projectId WHERE e.user.id = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unbindTaskFromProjectId(@NotNull final String userId, @NotNull final String id) {
        entityManager
                .createQuery(
                        "UPDATE TaskGraph e SET e.project.id = NULL WHERE e.user.id = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public @Nullable TaskGraph findOneByIdAndUserId(
            @Nullable final String userId, @Nullable final String id
    ) {
        return getSingleResult(entityManager
                .createQuery(
                        "SELECT e FROM TaskGraph e WHERE e.id = :id AND e.user.id = :userId", TaskGraph.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1));
    }

    @Override
    public @Nullable TaskGraph findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        List<TaskGraph> list = entityManager
                .createQuery(
                        "SELECT e FROM TaskGraph e WHERE e.name = :name AND e.user.id = :userId",
                        TaskGraph.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1).getResultList();
        if (list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public @Nullable TaskGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        List<TaskGraph> list = entityManager
                .createQuery("SELECT e FROM TaskGraph e WHERE e.user.id = :userId", TaskGraph.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index-1).setMaxResults(1).getResultList();
        if (list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskGraph e")
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.userId = :userId")
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeOneByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.user.id = :userId AND e.id =:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }


}
