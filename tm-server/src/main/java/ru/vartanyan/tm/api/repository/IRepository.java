package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.dto.AbstractEntity;

import javax.persistence.TypedQuery;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity);

    @NotNull E getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
